run-local:
	docker-compose -f docker-compose.yml -f docker-compose.local.yml up -d

run-dev:
	docker-compose -f docker-compose.yml -f docker-compose.dev.yml up -d

run-prod:
	docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d

stop:
	docker-compose down

install:
	docker-compose run node npm install

build:
	docker-compose run node npm run build

lint:
	docker-compose run node npm run lint
