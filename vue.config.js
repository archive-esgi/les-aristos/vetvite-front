process.env.VUE_APP_BASE_URL_API = process.env.BASE_URL_API;
process.env.VUE_APP_BASE_URL_API_PRISMA = process.env.BASE_URL_API_PRISMA;

module.exports = {
  devServer: {
    proxy: {
      "/api": {
          target: "http://172.17.0.1:8080",
          changeOrigin: true,
      },
      "/graphql": {
          target: "http://172.17.0.1:4000",
          changeOrigin: true
      },

    }
  },
  "transpileDependencies": [
    "vuetify"
  ]
}