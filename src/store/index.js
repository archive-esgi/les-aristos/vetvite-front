import Vue from 'vue';
import Vuex from 'vuex';
import Axios from '../plugins/axios';
// modules
import officeModule from './modules/officeModule';
import proModule from './modules/proModule';
import customerModule from './modules/customerModule';
import appointmentModule from './modules/appointmentModule';
import careTypeModule from './modules/careTypeModule';
import careModule from './modules/careModule'
import availabilityModule from './modules/availabilityModule';
import unavailabilityModule from './modules/unavailabilityModule';

Vue.use(Vuex);
Vue.use(Axios);

function parseJwt(token) {
    var base64Url = token.split('.')[1];
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    var jsonPayload = decodeURIComponent(atob(base64).split('').map(function (c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));

    return JSON.parse(jsonPayload);
}

export default new Vuex.Store({
    state: {
        status: '',
        crudError: '',
        token: localStorage.getItem('token') || '',
        user: {
            roles: []
        }
    },
    getters: {
        isLoggedIn: state => !!state.token,
        user: state => state.user,
        isVerified: state => state.user.enabled === 1,
        isAdmin: state => !!state.user.roles.includes("ROLE_ADMIN"),
        isPro: state => !!state.user.roles.includes("ROLE_PRO"),
        isClient: state => !!state.user.roles.includes("ROLE_CLIENT"),
        authStatus: state => state.status,
        getCrudError: state => state.crudError
    },
    // commited by actions
    mutations: {
        AUTH_REQUEST(state) {
            state.status = 'requested';
        },
        AUTH_SUCCESS(state, token) {
            state.status = 'success';
            state.token = token;
        },
        UPDATE_USER(state, user) {
            state.user = user;
        },
        UPDATE_USER_PROPS(state, obj) {
            state.user[obj.property] = obj.value;
        },
        AUTH_ERROR(state) {
            state.status = 'error';
        },
        LOGOUT(state) {
            state.status = '';
            state.token = '';
        },
        SET_CRUD_ERROR: state => state.crudError = 'Une erreur s\'est produite'
    },
    // triggered by store.dispatch
    actions: {
        login({commit}, user) {
            return new Promise((resolve, reject) => {
                commit('AUTH_REQUEST');
                Vue.prototype.$apiRest.post('/login', user)
                    .then(resp => {
                        const token = resp.data.token;
                        this.dispatch('loadCurrentUser', token)
                            .then(() => resolve(resp))
                            .catch(err => console.log(err));
                    })
                    .catch(err => {
                        commit('AUTH_ERROR');
                        localStorage.removeItem('token');
                        reject(err);
                    });
            });
        },
        register({commit}, user) {
            return new Promise((resolve, reject) => {
                commit('AUTH_REQUEST');
                Vue.prototype.$apiRest.post('/users', user)
                    .then(registrationResponse => {
                        this.dispatch('login', {email: user.email, password: user.plainPassword})
                            .then((loginResponse) => {
                                console.log('loginResponse', loginResponse);
                                resolve(registrationResponse);
                            })
                            .catch((err) => {
                                reject(err)
                            });
                    })
                    .catch(err => {
                        commit('AUTH_ERROR', err);
                        localStorage.removeItem('token');
                        reject(err);
                    });
            });
        },
        logout({commit}) {
            return new Promise((resolve) => {
                commit('LOGOUT');
                localStorage.removeItem('token');
                delete Vue.prototype.$apiRest.defaults.headers.common['Authorization'];
                resolve();
            });
        },
        loadCurrentUser({commit}, token) {
            return new Promise((resolve, reject) => {
                Vue.prototype.$apiRest.defaults.headers.common['Authorization'] = "Bearer " + token;
                const payload = parseJwt(token);
                Vue.prototype.$apiRest.get('/users/infos?email=' + payload.email).
                then(resp => {
                    if (resp.data['hydra:member'].length !== 1) {
                        reject("No user found !");
                        return
                    }
                    localStorage.setItem('token', token);
                    commit('AUTH_SUCCESS', token);
                    commit('UPDATE_USER', resp.data['hydra:member'][0]);
                    resolve();
                }).
                catch(err => {
                    commit('AUTH_ERROR');
                    delete Vue.prototype.$apiRest.defaults.headers.common['Authorization'];
                    localStorage.removeItem('token');
                    reject(err);
                });
            });
        },
        updateUser: ({commit}, data) => {
            return new Promise((resolve, reject) => {
                Vue.prototype.$apiRest.patch(`/users/${data.id}`, data, {
                    headers: {
                        'Content-Type': 'application/merge-patch+json'
                    }
                })
                    .then(res => {
                        commit('UPDATE_USER', res.data)
                        resolve(res.data);
                    })
                    .catch(err => {
                        commit('SET_CRUD_ERROR', err)
                        reject(err);
                    });
            })
        },
    },
    modules: {
        officeModule,
        proModule,
        customerModule,
        appointmentModule,
        careTypeModule,
        careModule,
        availabilityModule,
        unavailabilityModule
    }
})
