import Vue from 'vue';

export default {
    state: () => ({
        appointments: [],
        currentAppointments: [],
        appointmentDeleted: null
    }),
    getters: {
        getUserAppointments: (state) => state.currentAppointments
    },
    mutations: {
        SET_APPOINTMENTS: (state, appointments) => (state.appointments = appointments),
        SET_APPOINTMENT_ON_CURRENT_USER: (state, currentUserAppointments) => {
            return state.currentAppointments = currentUserAppointments;
        },
        UPDATE_APPOINTMENT: (state, updatedAppointment) => (state.currentAppointments = updatedAppointment),
        DELETE_APPOINTMENT(state, id) {
            const index = state.currentAppointments['hydra:member'].findIndex(appointment => appointment.id === id);
            state.currentAppointments['hydra:member'].splice(index, 1);
        },
        UPDATE_APPOINTMENT_DELETED: (state, id) => (state.appointmentDeleted = id),
    },
    actions: {
        loadAppointmentsByIdUser: async ({commit}, idUser) => {
            if( typeof idUser !== 'undefined' && typeof idUser === 'number' ) {
                return new Promise(resolve => {
                    Vue.prototype.$apiRest.get(`/appointments?client=${idUser}`)
                        .then(res => {
                            commit('SET_APPOINTMENT_ON_CURRENT_USER', res.data)
                            resolve();
                        })
                })
            }
        },
        deleteAppointment: ({commit}, id) => {
            return new Promise(resolve => {
                Vue.prototype.$apiRest.delete(`/appointments/${id}`)
                    .then(() => {
                        commit('DELETE_APPOINTMENT', id);
                        resolve();
                    })
            })
        },
        deleteAppointmentSaveId: ({commit}, id) => {
            return new Promise(resolve => {
                Vue.prototype.$apiRest.delete(`/appointments/${id}`)
                    .then(() => {
                        commit('UPDATE_APPOINTMENT_DELETED', id);
                        resolve();
                    })
            })
        },
        // A VOIR si on aura besoin d'effectuer ces action dans ce module-ci ou ailleurs
        /*
        loadAppointments: ({commit}) => {
            return new Promise(resolve => {
                Vue.prototype.$apiRest.get('/appointments')
                    .then(res => {
                        commit('SET_APPOINTMENTS', res.data)
                        resolve();
                    })
            })
        },

        loadAppointment: async ({commit}, id) => {
            return new Promise(resolve => {
                Vue.prototype.$apiRest.get(`/appointments/${id}`)
                    .then(res => {
                        commit('SET_APPOINTMENT', res.data)
                        resolve();
                    })
            })
        },

        updateppointment: ({commit}, data) => {
            return new Promise(resolve => {
                Vue.prototype.$apiRest.put(`/users/${data.id}`, data)
                    .then(res => {
                        commit('UPDATE_APPOINTMENT', res.data)
                        resolve();
                    })
            })
        }
        */
    }
}