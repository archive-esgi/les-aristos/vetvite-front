import Vue from 'vue';

export default {
    state: () => ({
        cares: [],
        currentCare: null,
        currentCares: []
    }),
    getters: {
        getCares: (state) => state.cares,
        getCurrentCare: (state) => state.currentCare,
        getCurrentCares: (state) => state.currentCares
    },
    mutations: {
        SET_CARES_ON_CURRENT_USER_PRO: (state, cares) => (state.cares = cares),
        SET_CARES_ON_CURRENT_USER: (state, cares) => (state.currentCares = cares),
        SET_CURRENT_CARE: (state, newCurrentCare) => {
            return state.currentCare = newCurrentCare;
        },
        UPDATE_CURRENT_CARE: (state, updatedCare) => (state.currentCare = updatedCare),
        DELETE_CARE(state, id) {
            const index = state.cares['hydra:member'].findIndex(care => care.id === id);
            state.cares['hydra:member'].splice(index, 1);
        },
        ADD_CARE: (state, addedCare) => state.cares.push(addedCare),
    },
    actions: {
        loadCaresByIdUserPro: async ({commit}, idUser) => {
            if (typeof idUser !== 'undefined' && typeof idUser === 'number') {
                return new Promise(resolve => {
                    Vue.prototype.$apiRest.get(`/cares?pro=${idUser}`)
                        .then(res => {
                            commit('SET_CARES_ON_CURRENT_USER_PRO', res.data)
                            resolve();
                        })
                })
            }
        },

        loadCaresByIdCurrentUserPro: async ({commit}, idUser) => {
            if (typeof idUser !== 'undefined' && typeof idUser === 'number') {
                return new Promise(resolve => {
                    Vue.prototype.$apiRest.get(`/cares?pro=${idUser}`)
                        .then(res => {
                            commit('SET_CARES_ON_CURRENT_USER', res.data['hydra:member'])
                            resolve();
                        })
                })
            }
        },

        deleteCare: ({commit}, id) => {
            return new Promise(resolve => {
                Vue.prototype.$apiRest.delete(`/cares/${id}`)
                    .then(() => {
                        commit('DELETE_CARE', id);
                        resolve();
                    })
            })
        },
        addCare: ({commit}, data) => {
            return new Promise((resolve, reject) => {
                console.log(data)
                Vue.prototype.$apiRest.post('/cares', data)
                    .then(res => {
                        console.log(res)
                        commit('ADD_CARE', res.data)
                        resolve();
                    })
                    .catch(err => {
                        commit('SET_CRUD_ERROR', err)
                        reject(err);
                    });
            })
        }
    }
}