import Vue from 'vue';

export default {
    state: () => ({
        customers: [],
        currentCustomer: null
    }),
    getters: {
        getCustomers: state => state.customers['hydra:member'],
        getCurrentCustomer: state => state.currentCustomer
    },
    mutations: {
        SET_CUSTOMERS: (state, customers) => (state.customers = customers),
        SET_CUSTOMER: (state, customer) => (state.currentCustomer = customer),
        UPDATE_CUSTOMER: (state, updatedCustomer) => {
            state.currentCustomer = updatedCustomer;
            const index = state.customers['hydra:member'].findIndex(customer => customer.id === updatedCustomer.id);
            if (index !== -1) {
                state.customers['hydra:member'][index] = updatedCustomer;
            }
        },
        ADD_CUSTOMER: (state, addedCustomer) => state.customers['hydra:member'].push(addedCustomer),
        DELETE_CUSTOMER(state, id) {
            const index = state.customers['hydra:member'].findIndex(customer => customer.id === id);
            state.customers['hydra:member'].splice(index, 1);
        },
    },
    actions: {
        loadCustomers: ({commit}) => {
            return new Promise((resolve, reject) => {
                Vue.prototype.$apiRest.get('/users?roles=CLIENT')
                    .then(res => {
                        commit('SET_CUSTOMERS', res.data)
                        resolve();
                    })
                    .catch(err => {
                        commit('SET_CRUD_ERROR', err)
                        reject(err);
                    });
            })
        },
        loadCustomer: ({commit}, id) => {
            return new Promise ((resolve, reject) => {
                Vue.prototype.$apiRest.get(`/users/${id}`)
                    .then(res => {
                        commit('SET_CUSTOMER', res.data)
                        resolve();
                    })
                    .catch(err => {
                        commit('SET_CRUD_ERROR', err)
                        reject(err);
                    });
            })
        },
        deleteCustomer: ({commit}, id) => {
            return new Promise((resolve, reject) => {
                Vue.prototype.$apiRest.delete(`/users/${id}`)
                    .then(() => {
                        commit('DELETE_CUSTOMER', id);
                        resolve();
                    })
                    .catch(err => {
                        commit('SET_CRUD_ERROR', err)
                        reject(err);
                    });
            })
        },
        updateCustomer: ({commit}, data) => {
            return new Promise((resolve, reject) => {
                Vue.prototype.$apiRest.patch(`/users/${data.id}`, data, {
                    headers: {
                        'Content-Type': 'application/merge-patch+json'
                    }
                })
                    .then(res => {
                        commit('UPDATE_CUSTOMER', res.data)
                        resolve();
                    })
                    .catch(err => {
                        commit('SET_CRUD_ERROR', err)
                        reject(err);
                    });
            })
        },
        addCustomer: ({commit}, data) => {
            return new Promise((resolve, reject) => {
                Vue.prototype.$apiRest.post('/users', data)
                    .then(res => {
                        commit('ADD_CUSTOMER', res.data)
                        resolve();
                    })
                    .catch(err => {
                        commit('SET_CRUD_ERROR', err)
                        reject(err);
                    });
            })
        }
    }
}