import Vue from 'vue';

export default {
    state: () => ({
        unavailabilities: [],
    }),
    getters: {
        proUnavailabilitites: state => state.unavailabilities,
    },
    mutations: {
        SET_UNAVAILABILITIES: (state, unavailabilities) => (state.unavailabilities = unavailabilities),
        ADD_UNAVAILABILITY: (state, unavailability) => state.unavailabilities.push(unavailability),
        DELETE_UNAVAILABILITY: (state, id) => {
            const index = state.unavailabilities.findIndex(unavailability => unavailability.id === id);
            state.unavailabilities.splice(index, 1);
        },
    },
    actions: {
        loadUnavailabilitites: ({commit}, userId) => {
            return new Promise((resolve, reject) => {
                Vue.prototype.$apiGraphql.post('',{
                    query: `
                        query {
                            unavailabilities (where: {
                                id_user: ${userId},
                            }) {id, start, stop, id_user}
                        }
                    `
                }).
                then(res => {
                    commit('SET_UNAVAILABILITIES', res.data.data.unavailabilities);
                    resolve(res.data.data.unavailabilities);
                }).
                catch(err => {
                    commit('SET_CRUD_ERROR', err)
                    reject(err);
                });
            })
        },
        removeUnavailability: ({commit}, unavailabilityId) => {
            return new Promise((resolve, reject) => {
                Vue.prototype.$apiGraphql.post('', {
                    query: `
                        mutation{
                            deleteUnavailability(where: {
                                id: "${unavailabilityId}"
                            }) { id }
                        }
                    `
                })
                    .then(() => {
                        commit('DELETE_UNAVAILABILITY', unavailabilityId);
                        resolve(unavailabilityId);
                    })
                    .catch(err => {
                        commit('SET_CRUD_ERROR', err)
                        reject(err);
                    });
            })
        },
        addUnavailabilitites: ({commit}, data) => {
            return new Promise((resolve, reject) => {
                Vue.prototype.$apiGraphql.post('', {
                    query: `mutation{
                        createUnavailability(data: {
                            start: "${data.start.format()}",
                            stop: "${data.stop.format()}",
                            created_at: "${data.createdAt.format()}",
                            id_user: ${data.user.id},
                        }) {id, start, stop, id_user}
                    }`
                }).
                then(res => {
                    res = res.data.data.createUnavailability
                    commit('ADD_UNAVAILABILITY', res);
                    resolve(res)
                }).
                catch(err => {
                    commit('SET_CRUD_ERROR', err);
                    reject(err);
                });
            })
        }
    }
}