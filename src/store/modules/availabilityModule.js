import Vue from 'vue';

export default {
    state: () => ({
        availabilities: [],
    }),
    getters: {
        proAvailabilities: state => state.availabilities,
    },
    mutations: {
        SET_AVAILABILITIES: (state, availabilities) => (state.availabilities = availabilities),
        ADD_AVAILABILITIES: (state, availabilities) => state.availabilities.push(...availabilities),
        DELETE_AVAILABILITY: (state, id) => {
            const index = state.availabilities.findIndex(availability => availability.id === id);
            state.availabilities.splice(index, 1);
        },
    },
    actions: {
        loadAvailabilities: ({commit}, userId) => {
            return new Promise((resolve, reject) => {

                Vue.prototype.$apiGraphql.post('',{
                    query: `
                        query {
                            availabilities (where: {
                                id_user: ${userId},
                            }) {id, day_number, id_user, id_office, hour_start}
                        }
                    `
                }).
                then(res => {
                    commit('SET_AVAILABILITIES', res.data.data.availabilities);
                    resolve(res.data.data.availabilities);
                }).
                catch(err => {
                    commit('SET_CRUD_ERROR', err)
                    reject(err);
                });
            })
        },
        removeAvailability: ({commit}, availabilityId) => {
            return new Promise((resolve, reject) => {
                Vue.prototype.$apiGraphql.post('', {
                    query: `
                        mutation{
                            deleteAvailability(where: {
                                id: "${availabilityId}"
                            }) { id }
                        }
                    `
                })
                    .then(() => {
                        commit('DELETE_AVAILABILITY', availabilityId);
                        resolve(availabilityId);
                    })
                    .catch(err => {
                        commit('SET_CRUD_ERROR', err)
                        reject(err);
                    });
            })
        },
        addAvailabilities: ({commit}, data) => {

            let req = '';
            data.forEach((avai, index) => {
                req += `r${index}: createAvailability(data: {
                           day_number: ${avai.dayNumber},
                           id_user: ${avai.user},
                           id_office: ${avai.office},
                           hour_start: "${avai.hour}"
                        }) {id, day_number, hour_start, id_user, id_office},`
                ;
            });

            return new Promise((resolve, reject) => {
                Vue.prototype.$apiGraphql.post('', {
                    query: `mutation{${req}}`
                }).
                then(res => {
                    res = Object.values(res.data.data);
                    commit('ADD_AVAILABILITIES', res);
                    resolve(res)
                }).
                catch(err => {
                    commit('SET_CRUD_ERROR', err);
                    reject(err);
                });
            })
        }
    }
}