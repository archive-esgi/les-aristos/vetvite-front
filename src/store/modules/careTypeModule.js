import Vue from 'vue';

export default {
    state: () => ({
        careTypes: [],
        currentCareType: null
    }),
    getters: {
        careTypes: state => state.careTypes,
        getCurrentCareType: state => state.currentCareType
    },
    mutations: {
        SET_CARETYPES: (state, careTypes) => (state.careTypes = careTypes),
        SET_CARETYPE: (state, careType) => (state.currentCareType = careType),
        UPDATE_CARETYPE: (state, updatedCareType) => (state.currentCareType = updatedCareType),
        ADD_CARETYPE: (state, addedCareType) => state.careTypes.push(addedCareType),
        DELETE_CARETYPE: (state, id) => {
            const index = state.careTypes.findIndex(careType => careType.id === id);
            state.careTypes.splice(index, 1);
        },
    },
    actions: {
        loadCareTypes: ({commit}) => {
            return new Promise((resolve, reject) => {
                Vue.prototype.$apiGraphql.post('',{
                    query: `
                        query {
                            careTypes {id, label, description}
                        }
                    `
                })
                .then(res => {
                    commit('SET_CARETYPES', res.data.data.careTypes);
                    resolve();
                })
                .catch((err) => {
                    commit ('SET_CRUD_ERROR', err)
                    reject(err);
                })
            })
        },
        loadCareType: ({commit}, id) => {
            return new Promise ((resolve, reject) => {
                Vue.prototype.$apiGraphql.post('', {
                    query: `
                        query {
                            careType (where: {
                                id: "${id}"
                            }){id, label, description, color_schedule}
                        }
                    `
                })
                .then(res => {
                    commit('SET_CARETYPE', res.data.data.careType)
                    resolve(res);
                })
                .catch(err => {
                    commit('SET_CRUD_ERROR', err)
                    reject(err);
                });
            })
        },
        deleteCareType: ({commit}, id) => {
            return new Promise((resolve, reject) => {
                Vue.prototype.$apiGraphql.post('', {
                    query: `
                        mutation{
                            deleteCareType(where: {
                                id: "${id}"
                            }) {
                                label,description
                            }
                        }
                        
                    `
                })
                .then(() => {
                    commit('DELETE_CARETYPE', id);
                    resolve();
                })
                .catch(err => {
                    commit('SET_CRUD_ERROR', err)
                    reject(err);
                });
            })
        },
        updateCareType: ({commit}, data) => {
            return new Promise((resolve, reject) => {
                Vue.prototype.$apiGraphql.post('', {
                    query: `
                        mutation {
                            updateCareType(
                                where: {
                                    id: "${data.id}",
                                },
                            data: {
                                    label: "${data.label}"
                                    description: "${data.description}"
                                }
                            ) {
                                label, description
                            }
                        }
                    `
                })
                .then(res => {
                    commit('UPDATE_CARETYPE', res.data.data.updateCareType)
                    resolve();
                })
                .catch(err => {
                    commit('SET_CRUD_ERROR', err)
                    reject(err);
                });
            })
        },
        addCareType: ({commit}, data) => {
            return new Promise((resolve, reject) => {
                Vue.prototype.$apiGraphql.post('', {
                    query: `
                        mutation{
                            createCareType(data: {
                                label: "${data.label}"
                                description: "${data.description}"
                            }) {
                                label,description
                            }
                        }
                    `
                })
                .then(res => {
                    commit('ADD_CARETYPE', res.data.data.createCareType);
                    resolve();
                })
                .catch(err => {
                    commit('SET_CRUD_ERROR', err);
                    reject(err);
                });
            })
        }
    }
}