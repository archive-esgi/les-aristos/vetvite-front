import Vue from 'vue';

export default {
    state: () => ({
        pros: [],
        currentPro: null,
    }),
    getters: {
        getPros: state => state.pros['hydra:member'],
        getCurrentPro: state => state.currentPro
    },
    mutations: {
        SET_PROS: (state, pros) => (state.pros = pros),
        SET_PRO: (state, pro) => (state.currentPro = pro),
        UPDATE_PRO: (state, updatedPro) => {
            state.currentPro = updatedPro;
            const index = state.pros['hydra:member'].findIndex(pro => pro.id === updatedPro.id);
            if (index !== -1) {
                state.pros['hydra:member'][index] = updatedPro;
            }
        },
        ADD_PRO: (state, addedPro) => state.pros['hydra:member'].push(addedPro),
        DELETE_PRO(state, id) {
            const index = state.pros['hydra:member'].findIndex(pro => pro.id === id);
            state.pros['hydra:member'].splice(index, 1);
        },
    },
    actions: {
        loadPros: ({commit}) => {
            return new Promise((resolve, reject) => {
                Vue.prototype.$apiRest.get('/users?roles=PRO&pagination=false')
                    .then(res => {
                        commit('SET_PROS', res.data)
                        resolve();
                    })
                    .catch(err => {
                        commit('SET_CRUD_ERROR', err)
                        reject(err);
                    });
            })
        },
        loadPro: ({commit}, id) => {
            return new Promise ((resolve, reject) => {
                Vue.prototype.$apiRest.get(`/users/${id}`)
                    .then(res => {
                        commit('SET_PRO', res.data)
                        resolve();
                    })
                    .catch(err => {
                        commit('SET_CRUD_ERROR', err)
                        reject(err);
                    });
            })
        },
        deletePro: ({commit}, id) => {
            return new Promise((resolve, reject) => {
                Vue.prototype.$apiRest.delete(`/users/${id}`)
                    .then(() => {
                        commit('DELETE_PRO', id);
                        resolve();
                    })
                    .catch(err => {
                        commit('SET_CRUD_ERROR', err)
                        reject(err);
                    });
            })
        },
        updatePro: ({commit}, data) => {
            return new Promise((resolve, reject) => {
                Vue.prototype.$apiRest.patch(`/users/${data.id}`, data, {
                    headers: {
                        'Content-Type': 'application/merge-patch+json'
                    }
                })
                .then(res => {
                    commit('UPDATE_PRO', res.data)
                    resolve();
                })
                .catch(err => {
                    commit('SET_CRUD_ERROR', err)
                    reject(err);
                });
            })
        },
        addPro: ({commit}, data) => {
            return new Promise((resolve, reject) => {
                Vue.prototype.$apiRest.post('/users', data)
                    .then(res => {
                        commit('ADD_PRO', res.data)
                        resolve();
                    })
                    .catch(err => {
                        commit('SET_CRUD_ERROR', err)
                        reject(err);
                    });
            })
        }
    }
}