import Vue from 'vue';

export default {
    state: () => ({
        offices: [],
        currentOffice: null
    }),
    getters: {
        officesOnWaiting: state => {
            if (state.offices.length === 0) return state.offices
            return state.offices.filter(office => office.status === 0)
        },
        officesOnPlaced: state => {
            if (state.offices.length === 0) return state.offices
            return state.offices.filter(office => office.status !== 0)
        },
        offices: state => state.offices,
        getCurrentOffice: state => state.currentOffice
    },
    mutations: {
        SET_OFFICES: (state, offices) => (state.offices = offices),
        SET_OFFICE: (state, office) => (state.currentOffice = office),
        UPDATE_OFFICE: (state, updateOffice) => {
            state.currentPro = updateOffice;
            const index = state.offices.findIndex(office => office.id === updateOffice.id);
            if (index !== -1) {
                state.offices[index] = updateOffice;
            }
        },
        ADD_OFFICE: (state, addedOffice) => state.offices.push(addedOffice),
        DELETE_OFFICE: (state, id) => {
            const index = state.offices.findIndex(office => office.id === id);
            state.offices.splice(index, 1);
        },
    },
    actions: {
        loadOffices: ({commit}) => {
            return new Promise((resolve, reject) => {
                Vue.prototype.$apiRest.get('/offices?pagination=false')
                    .then(res => {
                        commit('SET_OFFICES', res.data['hydra:member'])
                        resolve();
                    })
                    .catch(err => {
                        commit('SET_CRUD_ERROR', err)
                        reject(err);
                    });
            })
        },
        loadOffice: ({commit}, id) => {
            return new Promise ((resolve, reject) => {
                Vue.prototype.$apiRest.get(`/offices/${id}`)
                    .then(res => {
                        commit('SET_OFFICE', res.data)
                        resolve();
                    })
                    .catch(err => {
                        commit('SET_CRUD_ERROR', err)
                        reject(err);
                    });
            })
        },
        loadOfficesByPro: async ({commit}, proURI) => {
            return new Promise((resolve, reject) => {
                Vue.prototype.$apiRest.get(`/offices?pagination=false&pros=${proURI}`)
                    .then(response => {
                        let res = response.data['hydra:member'];

                        res = res.filter(function(elem, pos) {
                            return res.indexOf(elem) === pos;
                        })

                    res = res.filter((v,i,a) => a.findIndex(t =>(t.id === v.id)) === i)

                    commit('SET_OFFICES', res)
                    resolve(res);
                }).
                catch((err) => {
                    commit('SET_CRUD_ERROR', err)
                    reject(err);
                });
            })
        },
        deleteOffice: ({commit}, id) => {
            return new Promise((resolve, reject) => {
                Vue.prototype.$apiRest.delete(`/offices/${id}`)
                    .then((res) => {
                        commit('DELETE_OFFICE', id);
                        resolve(res);
                    })
                    .catch(err => {
                        commit('SET_CRUD_ERROR', err)
                        reject(err);
                    });
            })
        },
        updateOffice: ({commit}, data) => {
            return new Promise((resolve, reject) => {
                Vue.prototype.$apiRest.patch(`/offices/${data.id}`, data, {
                    headers: {
                        'Content-Type': 'application/merge-patch+json'
                    }
                })
                    .then(res => {
                        commit('UPDATE_OFFICE', res.data)
                        resolve(res.data);
                    })
                    .catch(err => {
                        commit('SET_CRUD_ERROR', err)
                        reject(err);
                    });
            })
        },
        addOffice: ({commit}, data) => {
            return new Promise((resolve, reject) => {
                Vue.prototype.$apiRest.post('/offices', data)
                    .then(res => {
                        commit('ADD_OFFICE', res.data)
                        resolve(res.data);
                    })
                    .catch(err => {
                        commit('SET_CRUD_ERROR', err)
                        reject(err);
                    });
            })
        }
    }
}