import Profile from "@/views/User/Profile";
import CustomerAppointments from "@/views/User/Appointments";

export default [
    {
        path: '/profile',
        name: 'MyProfile',
        component: Profile,
        meta: {
            requiresAuth: true
        }
    },
    {
        path: '/profile/appointments',
        name: 'CustomerAppointments',
        component: CustomerAppointments,
        meta: {
            requiresAuth: true
        }
    }
]
