import Error404 from '../views/Errors/404.vue';

export default [
    {
        path: '*',
        name: 'Error404',
        component: Error404
    }
];