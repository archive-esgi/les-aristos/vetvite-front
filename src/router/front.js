import Home from '../views/Home.vue';
import Search from '../views/Search.vue';

export default [
    {
        path: '/',
        name: 'Home',
        component: Home
    },
    {
        path: '/search',
        name: 'Search',
        component: Search,
        props: route => ({ query: route.query.q })
    }
];
