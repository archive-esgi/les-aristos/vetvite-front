import Vue from 'vue';
import VueRouter from 'vue-router';
import store from '../store';
import authenticationRoutes from './authentication';
import adminRoutes from './admin';
import errorRoutes from './error';
import frontRoutes from './front';
import customerRoutes from './customer';
import professionalRoutes from './professional';
import userRoutes from './user';


Vue.use(VueRouter);

const routes = [
    ...authenticationRoutes,
    ...userRoutes,
    ...customerRoutes,
    ...professionalRoutes,
    ...adminRoutes,
    ...frontRoutes,
    ...errorRoutes,
];

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
});

router.beforeEach((to, from, next) => {
    if(to.matched.some(record => record.meta.requiresAuth)) {
        if (store.getters.isLoggedIn) {
            next();
            return;
        }
        next({name: "Login"});
    }

    if(to.matched.some(record => record.meta.requiresAnonymous)) {
        if (!store.getters.isLoggedIn) {
            next();
            return;
        }
        next({name: "Error404"});
    }

    if(to.matched.some(record => record.meta.requiresAdmin)) {
        if (store.getters.isAdmin) {
            next();
            return;
        }
        next({name: "Error404"});
    }

    if(to.matched.some(record => record.meta.requiresPro)) {
        if (store.getters.isPro) {
            next();
            return;
        }
        next({name: "Error404"});
    }

    if(to.matched.some(record => record.meta.requiresClient)) {
        if (store.getters.isClient) {
            next();
            return;
        }
        next({name: "Error404"});
    }
    
    next();
});

export default router;
