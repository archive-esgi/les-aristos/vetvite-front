import Vue from "vue";

const Utils = {
    install: function (Vue) {
        Vue.prototype.$utils = {
            getChanges: this.getChanges,
            isEmptyObject: this.isEmptyObject,
            rules: this.rules,
        };
    },
    getChanges(source, target) {
        return Object.fromEntries(Object.entries({...source, ...target})
        .filter(([key, value]) => !Object.is(source[key], value)));
    },
    isEmptyObject(object) {
        return Object.keys(object).length === 0;
    },
    rules: {
        regex: {
            email: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
            text: /^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð '-]+$/,
            phoneNumber: /^(?:(?:\+|00)33|0)\s*[1-9](?:[\s.-]*\d{2}){4}$/,
            password: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/,
            postalCode: /^(?:0[1-9]|[13-8][0-9]|2[ab1-9]|9[0-5])(?:[0-9]{3})?|9[78][1-9](?:[0-9]{2})?$/,
        },
        required: value => !!value || 'Champs requis',
        min: value => value.length >= 8 || 'Ce champs doit contenir 8 caractères minimum !',
        max: value => value.length <= 30 || 'Ce champs doit contenir 30 caractères maximum !',
        email: value => Vue.prototype.$utils.rules.regex.email.test(value) || 'Merci de saisir une adresse email valide',
        text: value => Vue.prototype.$utils.rules.regex.text.test(value) || 'Ce champs de doit contenir que des lettres',
        phoneNumber: value => Vue.prototype.$utils.rules.regex.phoneNumber.test(value) || 'Merci de saisir un numéro de téléphone valide',
        password: value => Vue.prototype.$utils.rules.regex.password.test(value) || 'Votre mot de passe doit contenir des majuscules, miniscules, chiffres et caractères spéciaux',
        postalCode: value => Vue.prototype.$utils.rules.regex.postalCode.test(value) || 'Merci de saisir un code postale valide',
    }
}

export default Utils;
