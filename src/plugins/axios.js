import axios from 'axios';

axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';

const Axios = {
    install: function (Vue) {
        Vue.prototype.$apiRest = axios.create({
            baseURL: process.env.VUE_APP_BASE_URL_API
        })

        Vue.prototype.$apiGraphql = axios.create({
            baseURL: process.env.VUE_APP_BASE_URL_API_PRISMA,
        })
    }
}

export default Axios